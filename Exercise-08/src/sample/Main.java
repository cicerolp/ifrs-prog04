package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.controller.LoginController;

import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        // TODO (1) - Carregue a cena 'login' e exiba no primaryStage
        primaryStage.show();
    }

    private Scene loadLoginScene() throws IOException {
        FXMLLoader loader = new FXMLLoader();

        // TODO (2) - Encontre o local do arquivo 'LoginView.fxml' e sete em 'loader.setLocation'

        // No LoginView.fxml
        // TODO (3 - a) - Crie um layout utilizando somente VBox e HBox
        // TODO (3 - b) - Associe o View(LoginView) ao Controller(LoginController)
        // TODO (4) - Configure o id da caixa de texto do login
        // TODO (5) - Configure o id da caixa de texto da senha
        // TODO (6) - Configure o id do botão login
        // TODO (7) - COnfigure a ação 'onAction'

        Parent root = loader.load();
        Scene scene = new Scene(root);

        // TODO (11) - Passe uma referêcia à classe Main para o LoginController

        return scene;
    }

    private Scene loadOverviewScene() {
        return null;
    }

    public void handleLogin(String text, String passwordFieldText) {
        // TODO (13) - Verifique a validade do login/password
    }
}
