package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sample.AlertBox;
import sample.Main;
import sample.model.Person;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonOverviewController implements Initializable {
    Main main;

    @FXML
    private TableView<Person> personTable;

    @FXML
    private TableColumn<Person, String> firstNameColumn;

    @FXML
    private TableColumn<Person, String> lastNameColumn;

    @FXML
    private Label firstName;
    @FXML
    private Label lastName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstNameColumn.setCellValueFactory(elt -> elt.getValue().firstNameProperty());
        //firstNameColumn.setCellFactory(cell -> new PersonTableCellController());

        lastNameColumn.setCellValueFactory(elt -> elt.getValue().lastNameProperty());

        personTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetail(newValue));
    }

    public void setMain(Main main) {
        this.main = main;
        personTable.setItems(main.getPersonData());
    }

    private void showDetail(Person person) {
        if (person == null) {
            firstName.setText("");
            lastName.setText("");
        } else {
            firstName.setText(person.getFirstName());
            lastName.setText(person.getLastName());

            //PersonEditDialogController.display(person);
        }
    }

    @FXML
    private void handleDelete() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0 && selectedIndex < personTable.getItems().size()) {
            personTable.getItems().remove(selectedIndex);
        } else {
            AlertBox.display("Aviso", "Usuário inválido selecionado!");
        }
    }

    @FXML
    private void handleEdit() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();

        if (selectedIndex >= 0 && selectedIndex < personTable.getItems().size()) {
            //personTable.getItems().remove(selectedIndex);

            Person p = main.getPersonData().get(selectedIndex);

            boolean clicked = PersonEditDialogController.display(p);


            if (clicked) {
                firstName.setText(p.getFirstName());
                lastName.setText(p.getLastName());
            }

        } else {
            AlertBox.display("Aviso", "Usuário inválido selecionado!");
        }
    }

    @FXML
    private void handleNew() {
        Person p = new Person("", "");

        // retorna se usuário clicou Ok
        boolean clicked = PersonEditDialogController.display(p);

        if (clicked) {
            main.getPersonData().add(p);
        }
    }
}























