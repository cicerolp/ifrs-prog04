package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    Stage window;
    Button button;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        window.setTitle("Janela Principal");
        button = new Button("Close");

        window.setOnCloseRequest(evt -> {
            evt.consume();

            this.onClose();
        });

        button.setOnAction(evt -> this.onClose());

        StackPane layout = new StackPane();
        layout.getChildren().add(button);
        Scene scene = new Scene(layout, 300, 250);

        window.setScene(scene);
        window.show();
    }

    private void onClose() {
        boolean result = ConfirmBox.display("Atençao!", "Deseja fechar o programa?");

        if (result) {
            window.close();
        }
    }

}