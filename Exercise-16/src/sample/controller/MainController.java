package sample.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;
import sample.Main;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    Main main;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @FXML
    private void handleLogout() {
        main.handleLogout();
    }

    @FXML
    private void handleClose() {
        Platform.exit();
    }

    @FXML
    private void handleNew() {
        main.getPersonData().clear();
        main.setPersonFile(null);
    }

    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            main.loadFromFile(file);
        }
    }

    @FXML
    private void handleSave() {
        File file = main.getPersonFile();

        if (file != null) {
            main.saveToFile(file);
        } else {
            handleSaveAs();
        }
    }

    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            main.saveToFile(file);
        }
    }
}
