package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Main;
import sample.model.Person;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PersonEditDialogController implements Initializable {
    // OK(3) Declare 'TextField firstName' e anote com @FXML
    @FXML
    TextField firstName;
    // OK(4) Declare 'TextField lastName' e anote com @FXML
    @FXML
    TextField lastName;

    private Person mPerson;

    // OK(12-a) Declare 'boolean mClicked' e inicialize com 'false'
    private boolean mClicked = false;
    private Stage mWindow;

    // OK(13) Crie um método estático chamado 'display' que recebe uma 'Person' e retorna um 'boolean'
    public static boolean display(Person p) {
        // OK(14) Crie um bloco try-catch(IOException e)
        try {
            // OK(15-a) Utilizando 'FXMLLoader', carregue ''
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/fxml/PersonEditDialog.fxml"));
            // OK(15-b) Chame o método 'load' de FXMLLoader e armazene o retorno e uma variável 'Parent node'
            Parent node = loader.load();
            // OK(16) Crie um novo stage chamado 'window'
            Stage window = new Stage();
            // OK(17) Configure a modality de 'window' para 'Modality.APPLICATION_MODAL'
            window.initModality(Modality.APPLICATION_MODAL);

            // OK(18-a) Crie uma nova scene que recebe 'node' (new Scene(node))
            // OK(18-b) Chame o método 'setScene' de window
            Scene scene = new Scene(node);
            window.setScene(scene);

            // OK(19) Obtenha uma referência para o controler (loader.getController())
            PersonEditDialogController controller = loader.getController();
            // OK(20) Chame 'setPerson' do controller
            controller.setPerson(p);

            // OK(23) Chame 'setStage' do controller
            controller.setStage(window);

            // OK(24) Chame o método 'showAndWait' de window
            window.showAndWait();

            // OK(25) Retorne o valor de 'controller.mClicked'
            return controller.mClicked;


        } catch (IOException e) {
            System.out.println(e.toString());
        }
        // OK(26) No caso de algum erro, retorne falso
        return false;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    // OK(5) Crie um método chamamdo 'setPerson' que recebe uma 'Person' e retorna 'void'
    public void setPerson(Person p) {
        // OK(6) Crie uma variável de classe chamada 'mPerson' que armazena a 'Person' recebida no método
        mPerson = p;
        // OK(7) Atualize o conteúdo de 'firstName'
        firstName.setText(p.getFirstName());
        // OK(8) Atualize o conteúdo de 'lastName'
        lastName.setText(p.getLastName());
    }

    @FXML
    public void handleCancel() {
        mClicked = false;
        mWindow.close();
    }

    // OK(9) Crie um método chamado 'handleOk' que retorna 'void'
    @FXML
    public void handleOk() {
        // OK(10) Atualize o primeiro nome de 'mPerson'
        mPerson.setFirstName(firstName.getText());
        // OK(11) Atualize o último nome de 'mPerson'
        mPerson.setLastName(lastName.getText());
        // OK(12-b) Atualize o valor de 'mClicked' para 'true'
        mClicked = true;

        mWindow.close();
    }

    // OK(21) Crie um método chamado 'setStage' que recebe uma 'Stage'
    public void setStage(Stage window) {
        // OK(22) Crie uma variável de classe chamada 'mWindow' que armaneza o Stage do método
        mWindow = window;
    }
}