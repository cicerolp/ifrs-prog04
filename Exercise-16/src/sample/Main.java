package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.controller.LoginController;
import sample.controller.MainController;
import sample.controller.PersonOverviewController;
import sample.model.Person;
import sample.model.PersonListWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

public class Main extends Application {

    private final String title = "ADS - Programação IV";

    private Stage primaryStage;
    private BorderPane mainView;
    private ObservableList<Person> personData = FXCollections.observableArrayList();

    public static void main(String[] args) {
        try {
            launch(args);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(title);

        // initializes primary layout
        initMainView();

        //setScene(loadLoginView());
        handleLogin("admin", "admin");
    }

    public ObservableList<Person> getPersonData() {
        return personData;
    }

    public void setPersonData(ObservableList<Person> personData) {
        this.personData = personData;
    }

    private void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }

        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/icon/icon.png")));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/MainView.fxml"));

        mainView = loader.load();

        ((MainController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/LoginView.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadPersonOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/PersonOverviewView.fxml"));

        Parent root = loader.load();

        ((PersonOverviewController) loader.getController()).setMain(this);

        File file = getPersonFile();
        if (file != null) {
            loadFromFile(file);
        }

        return root;
    }

    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            try {
                mainView.setCenter(loadPersonOverviewView());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public void handleLogout() {
        // clear data
        personData.clear();

        try {
            setScene(loadLoginView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getPersonFile() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String path = prefs.get("file", null);

        if (path != null) {
            return new File(path);
        } else {
            return null;
        }
    }

    public void setPersonFile(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);

        if (file != null) {
            prefs.put("file", file.getPath());
            primaryStage.setTitle(title + " - " + file.getPath());
        } else {
            prefs.remove("file");
            primaryStage.setTitle(title);
        }
    }

    public void loadFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            PersonListWrapper wrapper = (PersonListWrapper) um.unmarshal(file);

            personData.clear();
            personData.addAll(wrapper.getPersons());

            primaryStage.setTitle(title + " - " + file.getPath());

            setPersonFile(file);

        } catch (JAXBException e) {
            file = null;
            setPersonFile(null);
        }
    }

    public void saveToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            PersonListWrapper wrapper = new PersonListWrapper();
            wrapper.setPersons(personData);

            m.marshal(wrapper, file);

            setPersonFile(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
