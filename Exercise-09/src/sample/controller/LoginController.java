package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.Main;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    Main main;

    @FXML
    private Button loginBtn;

    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passTextField;

    public void onLogin() {
        System.out.println("Login Clicked!");
        main.handleLogin(loginTextField.getText(), passTextField.getText());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Initialize LoginController");
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
