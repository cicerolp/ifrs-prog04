package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.controller.LoginController;
import sample.controller.MainController;
import sample.controller.PersonOverviewController;
import sample.model.Person;

import java.io.IOException;

public class Main extends Application {

    private Stage primaryStage;
    private BorderPane mainView;
    private ObservableList<Person> personData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("ADS - Programação IV");

        // initializes primary layout
        initMainView();

        //setScene(loadLoginView());
        handleLogin("admin", "admin");
    }

    public ObservableList<Person> getPersonData() {
        return personData;
    }

    public void setPersonData(ObservableList<Person> personData) {
        this.personData = personData;
    }

    private void setScene(Parent root) {
        if (primaryStage.getScene() == null) {
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
        } else {
            primaryStage.getScene().setRoot(root);
        }

        // TODO(09) - Adicione um ícone ao stage principal
        // primaryStage.getIcons().add(new Image(...))
        // ... Main.class.getResourceAsStream("../res/avatar0.png")
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void initMainView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/MainView.fxml"));

        mainView = loader.load();

        ((MainController) loader.getController()).setMain(this);
    }

    private Parent loadLoginView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/LoginView.fxml"));

        Parent root = loader.load();

        ((LoginController) loader.getController()).setMain(this);

        return root;
    }

    private Parent loadPersonOverviewView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/PersonOverviewView.fxml"));

        Parent root = loader.load();

        ((PersonOverviewController) loader.getController()).setMain(this);

        return root;
    }

    public boolean handleLogin(String text, String passwordFieldText) {
        if (text.equals("admin") && passwordFieldText.equals("admin")) {
            // load data from source
            personData.add(new Person("Angelo", "Fabris"));
            personData.add(new Person("Arlei", "Duarte"));
            personData.add(new Person("Christian", "Theobald"));
            personData.add(new Person("Gabriel", "Siqueira"));
            personData.add(new Person("Igor", "Kalb"));
            personData.add(new Person("Jean", "Pereira"));
            personData.add(new Person("Pedro", "Tessaro"));
            personData.add(new Person("William", "Grosseli"));

            try {
                mainView.setCenter(loadPersonOverviewView());
                setScene(mainView);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    public void handleLogout() {
        // clear data
        personData.clear();

        try {
            setScene(loadLoginView());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
